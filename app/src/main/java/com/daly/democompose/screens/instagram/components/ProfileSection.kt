package com.daly.democompose.screens.instagram.components

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringArrayResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.daly.democompose.R
import com.daly.democompose.utils.LightAndDarkModePreviews

@Composable
fun ProfileSection(
    modifier: Modifier = Modifier
) {
    Column(
        modifier = Modifier.fillMaxWidth()
    ) {
        Row(
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 20.dp)
        ) {
            RoundImage(
                painter = painterResource(id = R.drawable.daly),
                modifier = Modifier
                    .size(100.dp)
                    .weight(3f)
            )
            Spacer(modifier = Modifier.width(16.dp))
            StatSection(modifier = Modifier.weight(7f))
        }
        Spacer(modifier = Modifier.height(8.dp))
        ProfileDescription(
            displayName = stringResource(id = R.string.instagram_screen_profile_description_name),
            description = stringResource(id = R.string.instagram_screen_profile_description),
            url = stringResource(id = R.string.instagram_screen_profile_description_url),
            followedBy = stringArrayResource(id = R.array.instagram_screen_profile_description_followed_by).toList(),
            otherCount = 17
        )
    }
}

@LightAndDarkModePreviews
@Composable
fun ProfileSectionPreview() {
    ProfileSection()
}
