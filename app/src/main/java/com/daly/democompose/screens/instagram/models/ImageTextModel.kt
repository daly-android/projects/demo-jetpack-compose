package com.daly.democompose.screens.instagram.models

import androidx.compose.runtime.Stable
import androidx.compose.ui.graphics.painter.Painter

@Stable
data class ImageTextModel(
    val image: Painter,
    val text: String
)
