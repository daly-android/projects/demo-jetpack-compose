package com.daly.democompose.screens.instagram.components

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.daly.democompose.R

@Composable
fun StatSection(
    modifier: Modifier = Modifier
) {
    Row(
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.SpaceAround,
        modifier = modifier
    ) {
        ProfileStat(
            numberText = stringResource(id = R.string.instagram_screen_stat_section_posts_number),
            text = stringResource(id = R.string.instagram_screen_stat_section_posts_label)
        )

        ProfileStat(
            numberText = stringResource(id = R.string.instagram_screen_stat_section_followers_label),
            text = stringResource(id = R.string.instagram_screen_stat_section_followers_number)
        )

        ProfileStat(
            numberText = stringResource(id = R.string.instagram_screen_stat_section_following_label),
            text = stringResource(id = R.string.instagram_screen_stat_section_following_number)
        )
    }
}

@Composable
fun ProfileStat(
    numberText: String,
    text: String,
    modifier: Modifier = Modifier
) {
    Column(
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = modifier
    ) {
        Text(
            text = numberText,
            fontWeight = FontWeight.Bold,
            fontSize = 20.sp
        )
        Spacer(modifier = Modifier.height(4.dp))
        Text(text = text)
    }
}
