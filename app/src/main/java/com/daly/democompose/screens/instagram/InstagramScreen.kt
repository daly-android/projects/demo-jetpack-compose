package com.daly.democompose.screens.instagram

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.daly.democompose.R
import com.daly.democompose.screens.instagram.components.ButtonSection
import com.daly.democompose.screens.instagram.components.HighlightSection
import com.daly.democompose.screens.instagram.components.PostSection
import com.daly.democompose.screens.instagram.components.PostTabView
import com.daly.democompose.screens.instagram.components.ProfileSection
import com.daly.democompose.screens.instagram.components.TopBar
import com.daly.democompose.screens.instagram.models.ImageTextModel
import com.daly.democompose.utils.LightAndDarkModePreviews
import kotlinx.collections.immutable.toImmutableList

@Composable
fun InstagramScreen() {
    var selectedTabIndex by remember {
        mutableIntStateOf(0)
    }
    Column(
        modifier = Modifier.fillMaxSize()
    ) {
        TopBar(name = stringResource(id = R.string.instagram_screen_top_bar_back_label), modifier = Modifier.padding(10.dp))
        Spacer(modifier = Modifier.height(4.dp))
        ProfileSection()
        Spacer(modifier = Modifier.height(25.dp))
        ButtonSection(modifier = Modifier.fillMaxWidth())
        Spacer(modifier = Modifier.height(25.dp))
        HighlightSection(
            highlights = listOf(
                ImageTextModel(
                    image = painterResource(id = R.drawable.youtube),
                    text = stringResource(id = R.string.instagram_screen_highlights_youTube)
                ),
                ImageTextModel(
                    image = painterResource(id = R.drawable.qa),
                    text = stringResource(id = R.string.instagram_screen_highlights_q_and_a)
                ),
                ImageTextModel(
                    image = painterResource(id = R.drawable.discord),
                    text = stringResource(id = R.string.instagram_screen_highlights_discord)
                ),
                ImageTextModel(
                    image = painterResource(id = R.drawable.telegram),
                    text = stringResource(id = R.string.instagram_screen_highlights_telegram)
                )
            ).toImmutableList(),
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 20.dp)
        )
        Spacer(modifier = Modifier.height(10.dp))
        PostTabView(
            imageTextModels = listOf(
                ImageTextModel(
                    image = painterResource(id = R.drawable.ic_grid),
                    text = stringResource(id = R.string.instagram_screen_tab_view_posts)
                ),
                ImageTextModel(
                    image = painterResource(id = R.drawable.ic_reels),
                    text = stringResource(id = R.string.instagram_screen_tab_view_reels)
                ),
                ImageTextModel(
                    image = painterResource(id = R.drawable.ic_igtv),
                    text = stringResource(id = R.string.instagram_screen_tab_view_igtv)
                ),
                ImageTextModel(
                    image = painterResource(id = R.drawable.profile),
                    text = stringResource(id = R.string.instagram_screen_tab_view_profile)
                )
            )
        ) {
            selectedTabIndex = it
        }

        when (selectedTabIndex) {
            0 -> PostSection(
                posts = listOf(
                    painterResource(id = R.drawable.daly),
                    painterResource(id = R.drawable.tab_4),
                    painterResource(id = R.drawable.tab_2),
                    painterResource(id = R.drawable.daly_2),
                    painterResource(id = R.drawable.tab_3),
                    painterResource(id = R.drawable.tab_1)
                ),
                modifier = Modifier.fillMaxWidth()
            )
        }
    }
}

@LightAndDarkModePreviews
@Composable
fun ProfileScreenPreview() {
    InstagramScreen()
}
