package com.daly.democompose.di

import com.daly.democompose.core.implementations.ResourcesRepositoryImpl
import com.daly.democompose.core.interfaces.ResourcesRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class ResourcesRepositoryModule {

    @Binds
    @Singleton
    abstract fun bindResourcesRepository(
        resourcesRepositoryImpl: ResourcesRepositoryImpl
    ): ResourcesRepository
}
