package com.daly.democompose.di

import com.daly.democompose.core.implementations.DispatcherServiceImpl
import com.daly.democompose.core.interfaces.DispatcherService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    @Singleton
    fun provideDispatcherPService(): DispatcherService {
        return DispatcherServiceImpl()
    }
}
