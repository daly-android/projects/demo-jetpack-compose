package com.daly.democompose

import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import com.daly.democompose.screens.instagram.InstagramDemoActivity
import com.daly.democompose.system_design.theme.DemoComposeTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        installSplashScreen()

        setContent {
            DemoComposeTheme {
                val context = LocalContext.current
                Surface(modifier = Modifier.fillMaxSize(), color = MaterialTheme.colorScheme.background) {
                    Box {
                        Button(
                            onClick = {
                                context.startActivity(Intent(context, InstagramDemoActivity::class.java))
                            },
                            modifier = Modifier.align(Alignment.Center)
                        ) {
                            Text(text = stringResource(id = R.string.go_instagram_screen_button))
                        }
                    }
                }
            }
        }
    }
}
