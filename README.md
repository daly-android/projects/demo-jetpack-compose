# Demo Jetpack Compose App

# Overview
This is an application demo for Jetpack compose samples.

It present some Jetpack compose realisation such as static screens, basic components, animations ...

## Instagram profile screen
|                                                                                   |                                                                                 |
|:---------------------------------------------------------------------------------:|:-------------------------------------------------------------------------------:|
| <img width="1604" alt="instagram_light" src="docs/instagram/instagram_light.png"> | <img width="1604" alt="instagram_dark" src="docs/instagram/instagram_dark.png"> |